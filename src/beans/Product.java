/*
 * Authors: Connor, Mick
 */
package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Connor, Mick
 *
 */
@ManagedBean
@ViewScoped
/*
 * Product: holds information for client-side product display 
 */
public class Product {

	@Size(message = "Please fill out all forms", min = 1)
	@NotNull(message = "Don't be null, be cool")
	private String name, description, image;
	@Size(message = "Please fill out all forms", min = 1)
	@NotNull(message = "Don't be null, be cool")
	private String price;
	private int id;

	/**
	 * Default Product constructor
	 */
	public Product() {
		this.name = "Default Product";
		this.description = "Default product description";
		this.image = "default.png";
		this.price = "$2.00";
		this.id = 0;
	}

	/**
	 * Product constructor
	 * @param name
	 * @param description
	 * @param image
	 * @param price
	 * @param id
	 */
	public Product(String name, String description, String image, String price, int id) {
		this.name = name;
		this.description = description;
		this.image = image;	// imageName.type; no directory information
		this.price = price;
		this.id = id;
	}

	/**
	 * Name
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Name
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Description
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the name (with file type) of a product image
	 * @return String
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Image string
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Price
	 * @return String
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * Price
	 * @param price
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * ID
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * ID
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

}
