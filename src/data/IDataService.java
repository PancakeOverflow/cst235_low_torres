package data;

import java.util.List;

import javax.ejb.Local;

@Local
public interface IDataService<E> {
	// CRUD methods
	public boolean create(E o);

	public E read(E o);

	public boolean update(E o);

	public boolean delete(E o);

	public List<E> selectAll();
}
