package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;

import beans.User;

@Stateless
@Local(IDataService.class)
public class UserDataService implements IDataService<User> {

	Connection conn;
	String url;

	public UserDataService() {

	}

	@Override
	public boolean create(User user) {
		String sql = "insert into USER(EMAIL, PASSWORD, FIRST_NAME, LAST_NAME) values ('" + user.getEmail() + "', '"
				+ user.getPassword() + "', '" + user.getFirstName() + "', '" + user.getLastName() + "')";
		boolean status;
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Create User successful");
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Create User failed. Query=" + sql);
		} finally {
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public User read(User user) {
		ResultSet result = null;
		User resultUser = null;
		String sql = "select * from USER where EMAIL = '" + user.getEmail() + "' and PASSWORD = '" + user.getPassword()
				+ "'";
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);
			while (result.next()) {
				resultUser = new User(result.getString("FIRST_NAME"), result.getString("LAST_NAME"),
						result.getString("EMAIL"), result.getString("PASSWORD"), result.getInt("ID"));
			}
			System.out.println("#: Successfully matched user credentials with DB entry. Log in may continue.");
		} catch (SQLException e) {
			System.out.println("#! Failed to find User. Query=" + sql);
		} finally {
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultUser;
	}

	@Override
	public boolean update(User user) {
		System.out.println("#: Update called on user: " + user.toString());
		String sql = "update USER where ID = " + user.getId() + " set EMAIL = '" + user.getEmail() + "', PASSWORD = '"
				+ user.getPassword() + "', FIRST_NAME = '" + user.getFirstName() + "', LAST_NAME = '"
				+ user.getLastName() + "')";
		boolean status;
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Update User successful");
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update User failed. Query=" + sql);
		} finally {
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public boolean delete(User user) {
		System.out.println("#: Delete called on user: " + user.toString());
		String sql = "delete from USER where EMAIL = '" + user.getEmail() + "' and PASSWORD = '" + user.getPassword()
				+ "' and FIRST_NAME = '" + user.getFirstName() + "' and LAST_NAME = '" + user.getLastName() + "')";
		boolean status;
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Delete User successful");
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Delete User failed Query=" + sql);
		} finally {
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public List<User> selectAll() {
		ResultSet result = null;
		List<User> resultList = null;
		String sql = "select * from USER";
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);
			System.out.println("#: Successfully returned a ResultSet containing USER entries. Converting to User list:");
			resultList = new ArrayList<>();
			while(result.next() ) {
				User u = new User(result.getString("FIRST_NAME"), result.getString("LAST_NAME"),
						result.getString("EMAIL"), result.getString("PASSWORD"), result.getInt("ID"));
				resultList.add(u);				
			}
		} catch (SQLException e) {
			System.out.println("#! Failed to select Users. Query=" + sql);
		} finally {
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}

}
