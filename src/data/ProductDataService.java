package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Product;

/*
 * When creating objects that modify a database, this interface should be implemented (CRUD functionality)
 */
public class ProductDataService implements IDataService<Product> {

	Connection conn;
	String url;

	public ProductDataService() {
		// TODO Auto-generated constructor stub
	}

	// CRUD
	@Override
	public boolean create(Product product) {
		String sql = "insert into PRODUCT(NAME, PRICE, DESCRIPTION, IMAGE) values ('" + product.getName() + "', '"
				+ product.getPrice() + "', '" + product.getDescription() + "', '" + product.getImage() + "')";
		boolean status;
		try {
			String url = "jdbc:mysql://localhost/pastrami?o=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Inserted product: " + product.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Create Product failed. Query=" + sql);
		} finally {
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public Product read(Product product) {
		ResultSet result = null;
		Product resultProduct = null;
		String sql = "select * from PRODUCT where ID = '" + product.getId() + "'";
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);
			while (result.next()) {
				resultProduct = new Product(result.getString("NAME"), result.getString("DESCRIPTION"),
						result.getString("IMAGE"), result.getString("PRICE"), result.getInt("ID"));
			}
			System.out.println("#: Selected product : " + resultProduct.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find User. Query=" + sql);
		} finally {
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultProduct;
	}

	@Override
	public boolean update(Product product) {
		String sql = "update PRODUCT set NAME = '" + product.getName() + "', PRICE = '" + product.getPrice()
				+ "', DESCRIPTION = '" + product.getDescription() + "', IMAGE = '" + product.getImage()
				+ "' where ID = " + product.getId();
		boolean status;
		try {
			String url = "jdbc:mysql://localhost/pastrami?o=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Updated product: " + product.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update Product failed. Query=" + sql);
		} finally {
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public boolean delete(Product product) {
		String sql = "delete from PRODUCT where ID = " + product.getId();
		boolean status;
		try {
			String url = "jdbc:mysql://localhost/pastrami?o=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Updated product: " + product.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update Product failed. Query=" + sql);
		} finally {
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public List<Product> selectAll() {
		ResultSet result = null;
		List<Product> resultList = new ArrayList<>();
		String sql = "select * from PRODUCT";
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);
			while (result.next()) {
				resultList.add(new Product(result.getString("NAME"), result.getString("DESCRIPTION"),
						result.getString("IMAGE"), result.getString("PRICE"), result.getInt("ID")));
			}
			System.out.println("#: Selected products : " + resultList.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find any Products. Query=" + sql);
		} finally {
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}

}
