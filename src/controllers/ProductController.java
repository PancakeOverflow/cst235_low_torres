/*
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package controllers;

import java.sql.SQLException;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import beans.Product;
import business.IProductManager;
import business.ProductBusinessService;

/*
 * this is our ProductController class. Inside of the class we have three methods
 * named onSubmit, cancel, and getService from our IProductManager Interface.
 */

@ManagedBean
@ViewScoped
public class ProductController {

	@Inject
	IProductManager service;
	
	ProductBusinessService business;
	
	/**
	 * onSubmit() allows the user (in the admin page) to add a product to the
	 * product list.
	 */
	public String onSubmit() {

		// Get the User Managed Bean
		FacesContext context = FacesContext.getCurrentInstance();
		Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);
		
		// add product to manager
		service.addProduct(product);
		
		// update DB
		business = new ProductBusinessService(product);
		business.createProduct();
		
		// Forward to Test Response View along with the User Managed Bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
		return "ProductList.xhtml";
	}

	/**
	 * cancel() will allow the admin to cancel from making a product and return the
	 * admin back to the AdminHome.xhtml.
	 */
	public String cancel() {
		return "AdminHome.xhtml";
	}

	/**
	 * getService() returns an IProductManager Object.
	 */
	public IProductManager getService() {
		return service;
	}
	
	public List<Product> getProducts() throws SQLException {
		return business.selectAllProducts();
	}
}
