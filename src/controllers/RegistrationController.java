
/*
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.User;
import business.UserBusinessService;

/**
 * this is our RegistrationController class.
 */

@ManagedBean
@ViewScoped
public class RegistrationController {

	/**
	 * This is our onSubmit() method that allows the user to register within our
	 * site and redirects the user to the main user home page.
	 */
	public String onSubmit() {
		// getting the User managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		// taking user managed bean with the file
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		UserBusinessService service = new UserBusinessService(user);

		if (service.registerUser())
			return "HomePage.xhtml";
		System.out.println("#! Failed to Register");
		return "Register.xhtml";

	}
}
