/*
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package controllers;

import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.User;
import business.UserBusinessService;

/**
 * this is our LoginController class.
 */

@ManagedBean
@ViewScoped
public class LoginController {

	/**
	 * Inside of the class we have a method named onSubmit() that allows the user to
	 * login within our site and redirects the user to the main user home page.
	 * Since the database is not fully connected yet, we only have one account that
	 * is able to fully log in. Anything other entry would redirect the user to our
	 * error.xhtml page.
	 * 
	 * @return xhtml file
	 * @throws SQLException 
	 */
	public String onSubmit() throws SQLException {
		// getting the User managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		// taking user managed bean with the file
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);

		UserBusinessService service = new UserBusinessService(user);
		user = service.loginUser();
		if (user == null || user.getId() < 1)
			return "Error.xhtml";
		else
			return "HomePage.xhtml";
	}
}
