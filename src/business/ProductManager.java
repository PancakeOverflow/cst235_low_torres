/*
 * Authors: Connor, Mick
 */
package business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Product;

/*
 * Holds an array of Product objects. 
 */
@Stateless
@Local(IProductManager.class)
@Alternative
public class ProductManager implements IProductManager {

	private List<Product> products = new ArrayList<Product>();

	/**
	 * ProductManager constructor
	 * @param products
	 */
	public ProductManager(List<Product> products) {
		super();
		this.products = products;
		System.out.println("#: ProjectManager object created with a List<Product>.");
	}
	
	/**
	 * Default ProductManager constructor
	 */
	public ProductManager() {
		System.out.println("#: Default ProjectManager object created");
	}

	@Override
	public void setProducts(List<Product> productList) {
		
		this.products = productList;
	}

	@Override
	public List<Product> getProducts() {
		return products;
	}



	@Override
	public void addProduct(Object p) {
		if (p instanceof Product) {
			products.add((Product) p);
			System.out.println("#: Added product (id:" + ((Product) p).getId() + ").");
		} else
			System.out.println("#: Failed to add product. Invalid object type: " + p.getClass().toString());
	}

	@Override
	public void removeProduct(Object p) {
		if (p instanceof Product) {
			products.remove((Product) p);
			System.out.println("#:Removed product (id:" + ((Product) p).getId() + ").");
		} else
			System.out.println("#: Failed to add product. Invalid object type: " + p.getClass().toString());
	}

}
