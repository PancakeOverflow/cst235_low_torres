package business;

import java.sql.SQLException;
import java.util.List;

import beans.Product;
import data.ProductDataService;

public class ProductBusinessService {

	private Product product;

	public ProductBusinessService(Product product) {
		this.product = product;
	}

	public void createProduct() {
		if (product != null && product.getName().length() <= 200 && product.getImage().length() <= 255) {
			ProductDataService service = new ProductDataService();
			service.create(product);
		}
	}

	public void deleteProduct() {
		if (product != null && product.getName().length() <= 200 && product.getImage().length() <= 255) {
			ProductDataService service = new ProductDataService();
			service.delete(product);
		}
	}

	public void updateProduct() {
		if (product != null && product.getName().length() <= 200 && product.getImage().length() <= 255) {
			ProductDataService service = new ProductDataService();
			service.update(product);
		}
	}

	public List<Product> selectAllProducts() throws SQLException {
		ProductDataService service = new ProductDataService();
		List<Product> productArray = service.selectAll();
		System.out.println("#: SelectAll found " + productArray.size() + " products.");
		return productArray;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product
	 *            the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

}
