package business;

import javax.ejb.Local;
import javax.ejb.Stateless;

import beans.User;
import data.UserDataService;

@Stateless
@Local
public class UserBusinessService {
	private User user;

	public UserBusinessService() {

	}

	public UserBusinessService(User user) {
		this.user = user;
	}

	/**
	 * Validates User email and password with Database
	 * 
	 * @return User
	 */
	public User loginUser() {
		System.out.println("#: loginUser() called");
		if (user.getEmail().length() > 3 && user.getPassword().length() > 4) {
			UserDataService service = new UserDataService();
			System.out.println("#: returning result set...");
			user = service.read(user);
			return user;
		}
		System.out.println(
				"#! loginUser() failed: rules broken: email=" + user.getEmail() + " password=" + user.getPassword());
		return null;
	}

	/**
	 * Validates User object and then sends information to a UserDataService 
	 * @return boolean
	 */
	public boolean registerUser() {
		System.out.println("#: registerUser() called");
		if (user.getEmail().length() > 3 && user.getPassword().length() > 4 && user.getFirstName().length() > 1
				&& user.getLastName().length() > 1 && user.getEmail().length() < 100
				&& user.getPassword().length() < 100 && user.getFirstName().length() < 100
				&& user.getLastName().length() < 100) {
			UserDataService service = new UserDataService();
			System.out.println("#: attempting to add User");
			return service.create(user);
		}
		System.out.println("#! loginUser() failed: rules broken: email=" + user.getEmail() + " password="
				+ user.getPassword() + " firstName=" + user.getFirstName() + " lastname=" + user.getLastName());
		return false;
	}

}
