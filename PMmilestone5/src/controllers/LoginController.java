/*
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package controllers;

import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.User;
import business.UserBusinessService;

/**
 * Handles Login requests
 */
@ManagedBean
@ViewScoped
public class LoginController {

	private UserBusinessService service;
	/**
	 * Allows the user to login within our site and redirects the user to the main
	 * user home page.
	 * 
	 * @return xhtml file
	 * @throws SQLException
	 */
	public String onSubmit() throws SQLException {
		System.out.println("#: LoginController.onSubmit()");

		// getting the User managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		// taking user managed bean with the file
		service = new UserBusinessService(user);
		user = service.loginUser();

		// return correct view and update context
		if (user == null || user.getId() < 1) {
			return "Error.xhtml";
		} else {
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
			return "HomePage.xhtml";
		}
	}
}
