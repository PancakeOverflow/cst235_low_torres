/**
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.Product;
import business.ProductBusinessService;

/**
 * This is our ProductController class. Inside of the class we have three
 * methods named onSubmit, cancel, and getService from our IProductManager
 * Interface.
 */
@ManagedBean
@ViewScoped
public class UpdateProductController {

	private ProductBusinessService business;

	/**
	 * Directs user to a Product Form using the id parameter.
	 * 
	 * @param id
	 * @return String
	 */
	public String getProductView(int id) {
		if (id < 1) {
			System.out.println("#! Something went wrong: the id is invalid.");
			return "";
		}

		// Get the Product
		Product product = new Product(id);
		business = new ProductBusinessService(product);
		product = business.selectProduct();
		System.out.println("#: Retrieved product: " + product.toString());

		// Forward to the product editor view
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
		return "UpdateProductForm.xhtml";
	}

	/**
	 * Calls the updateProduct method on a ProductBusinessService using a form for
	 * context. User is redirected to the ProductList.xhtml
	 * 
	 * @return String
	 */
	public String updateProduct() {

		// getting the product managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);

		// taking product managed bean with the file
		business = new ProductBusinessService(product);
		business.updateProduct();

		if (product == null || product.getId() < 1) {
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
			return "Error.xhtml"; // TODO leads to login error
		}
		return "ProductList.xhtml";
	}

	/**
	 * User is redirected to the ProductList.xhtml
	 * 
	 * @return String
	 */
	public String cancel() {
		return "ProductList.xthml";
	}

	/**
	 * Calls the deleteProduct method on a ProductBusinessService using a form for
	 * context. User is redirected to the ProductList.xhtml
	 * 
	 * @return String
	 */
	public String deleteProduct() {

		// getting the product managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);

		// taking product managed bean with the file
		business = new ProductBusinessService(product);
		business.deleteProduct();
		System.out.println("#: Product is being deleted");

		if (product == null || product.getId() < 1) {
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
			return "Error.xhtml"; // TODO leads to login error
		}

		return "ProductList.xhtml";
	}
}
