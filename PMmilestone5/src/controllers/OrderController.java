package controllers;

import javax.faces.bean.*;
import javax.faces.context.FacesContext;

import beans.Order;
import business.*;

/**
 * 
 * Controller for handling Orders
 */
@ManagedBean
@ViewScoped
public class OrderController {

	private OrderBusinessService service;

	/**
	 * Creates an order and returns the ConfirmationOrder.xhtml view.
	 * 
	 * @param productName
	 * @param productPrice
	 * @param firstName
	 * @param email
	 * @return String
	 */
	public String createOrder(String productName, String productPrice, String firstName, String email) {

		// Create an order with parameters passed in.
		Order order = new Order(productName, firstName, email, productPrice);

		// Update Database with Order
		service = new OrderBusinessService(order);
		service.createOrder();
		System.out.println("#: Retrieved Order: " + order.toString());

		// Return view
		return "ConfirmationOrder.xhtml";
	}

	/**
	 * Updates an order and returns the BrowseProducts.xhtml view
	 * 
	 * @return String
	 */
	public String updateOrder() {

		// Get order from context
		FacesContext context = FacesContext.getCurrentInstance();
		Order order = context.getApplication().evaluateExpressionGet(context, "{#order}", Order.class);

		// Avoid invalid orders
		if (order == null || order.getId() < 1) {
			return "Error.xhtml"; // TODO leads to login error
		}
		// Update order in database via business service
		service = new OrderBusinessService(order);
		service.updateOrder();

		// Update Order in context and return view
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("order", order);
		return "BrowseProducts.xhtml";

	}

	/**
	 * Deletes an order and returns the BrowseProducts.xhtml view.
	 * 
	 * @return String
	 */
	public String deleteOrder() {
		System.out.println("#: Order is being deleted");

		// Get order from context
		FacesContext context = FacesContext.getCurrentInstance();
		Order order = context.getApplication().evaluateExpressionGet(context, "{#order}", Order.class);

		// avoid invalid orders
		if (order == null || order.getId() < 1) {
			return "Error.xhtml"; // TODO leads to login error
		}
		// Create new order in database using business service.
		service = new OrderBusinessService(order);
		service.deleteOrder();
		
		// update order in context and return view.
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("order", order);
		return "BrowseProducts.xhtml";
	}

}
