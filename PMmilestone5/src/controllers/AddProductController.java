/*
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import beans.Product;
import business.ProductBusinessService;

/**
 * this is our ProductController class. Inside of the class we have three
 * methods named onSubmit, cancel, and getService from our IProductManager
 * Interface.
 */
@ManagedBean
@ViewScoped
public class AddProductController {

	private ProductBusinessService business;

	/**
	 * onSubmit() allows the user (in the admin page) to add a product to the
	 * product list.
	 * 
	 * @return Location: ProductList.xhtml
	 */
	public String onSubmit() {

		// Get the User Managed Bean
		FacesContext context = FacesContext.getCurrentInstance();
		Product product = context.getApplication().evaluateExpressionGet(context, "#{product}", Product.class);

		System.out.println("#: Inserting product: " + product.toString());

		// update DB
		business = new ProductBusinessService(product);
		business.createProduct();

		// Forward to Test Response View along with the User Managed Bean
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("product", product);
		return "ProductList.xhtml";
	}

	/**
	 * Cancel product creation and redirect the admin back to the AdminHome.xhtml.
	 * 
	 * @return location: AdminHome.xhtml
	 */
	public String cancel() {
		return "AdminHome.xhtml";
	}
}
