package data;

import java.util.List;

import javax.ejb.Local;

@Local
public interface IDataService<E> {
	// CRUD methods
	/**
	 * Execute a SQL insert command using object properties.
	 * @param o
	 * @return
	 */
	public boolean create(E o);

	/**
	 * Execute a SQL select command using object id. Returns an object.
	 * @param o
	 * @return
	 */
	public E read(E o);

	/**
	 * Execute a SQL update command using object properties.
	 * 
	 * @param o
	 * @return
	 */
	public boolean update(E o);

	/**
	 * Execute a SQL delete command using object id.
	 * 
	 * @param o
	 * @return
	 */
	public boolean delete(E o);

	/**
	 * Execute a SQL select command. Returns a list of objects.
	 * 
	 * @return List<E>
	 */
	public List<E> selectAll();
}
