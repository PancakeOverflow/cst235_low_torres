package data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.*;
import javax.enterprise.inject.Alternative;

import beans.Order;

/**
 * When creating objects that modify a database, this interface should be
 * implemented (CRUD functionality)
 * 
 * @see IDataService
 */
@Stateless
@Local(IDataService.class)
public class OrderDataService implements IDataService<Order> {

	private Connection conn;

	/**
	 * CRUD and FindAll methods for ORDERS table
	 */
	public OrderDataService() {
		// TODO Auto-generated constructor stub
		System.out.println("#: OrderDataService created");
	}

	// CRUD
	@Override
	public boolean create(Order order) {
		System.out.println("#: OrderDataService.create(" + order.toString() + ")");

		// create query
		String sql = "insert into ORDERS(PRODUCT, TOTAL, DESCRIPTION, FIRST_NAME, EMAIL) values ('" + order.getProduct()
				+ "', '" + order.getPrice() + "', '" + order.getDescription() + "', '" + order.getFirstName() + "', '"
				+ order.getEmail() + "')";
		boolean status;
		try {

			// establish connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			System.out.println("#: OrderDataService => Connection open.");

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Inserted order: " + order.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Create Order failed. Query=" + sql);
		} finally {
			// try to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public Order read(Order order) {
		System.out.println("#: OrderDataService.read(" + order.toString() + ")");

		// create query and resultSet
		ResultSet result = null;
		Order resultOrder = null;
		String sql = "select * from ORDERS where ID = '" + order.getId() + "'";
		try {

			// connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);

			// if successful, create Order for return
			if (result.next()) {
				resultOrder = new Order(result.getString("PRODUCT"), result.getString("FIRST_NAME"),
						result.getString("EMAIL"), result.getString("DESCRIPTION"),
						result.getTimestamp("DATE").toString(), "" + result.getDouble("TOTAL"));
				resultOrder.setId(result.getInt("ID"));
			}
			System.out.println("#: Selected order : " + resultOrder.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find User. Query=" + sql);
		} finally {

			// try to close the connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultOrder;
	}

	@Override
	public boolean update(Order order) {
		System.out.println("#: OrderDataService.update(" + order.toString() + ")");

		// create query
		String sql = "update ORDERS set PRODUCT = '" + order.getProduct() + "', TOTAL = '" + order.getPrice()
				+ "', DESCRIPTION = '" + order.getDescription() + "', FIRST_NAME = '" + order.getFirstName()
				+ "', EMAIL = '" + order.getEmail() + "', DATE = NOW() where ID = " + order.getId();
		boolean status;
		try {

			// connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Updated order: " + order.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update Order failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public boolean delete(Order order) {
		System.out.println("#: OrderDataService.delete(" + order.toString() + ")");

		// create query
		String sql = "delete from ORDERS where ID = " + order.getId();
		boolean status;
		try {

			// attempt to connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Updated order: " + order.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update Order failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public List<Order> selectAll() {
		System.out.println("#: OrderDataService.selectAll()");

		// create resultSet and query
		ResultSet result = null;
		List<Order> resultList = new ArrayList<>();
		String sql = "select * from ORDERS";
		try {

			// connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);

			// iterate over results and build resultList
			while (result.next()) {
				Order order = new Order(result.getString("PRODUCT"), result.getString("FIRST_NAME"),
						result.getString("EMAIL"), result.getString("DESCRIPTION"),
						result.getTimestamp("DATE").toString(), "" + result.getDouble("TOTAL"));
				order.setId(result.getInt("ID"));
				resultList.add(order);
			}
			System.out.println("#: Selected orders : " + resultList.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find any Orders. Query=" + sql);
		} finally {

			// try to close connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public List<Order> selectAll(String name) {
		System.out.println("#: OrderDataService.selectAll()");

		// build query and resultList
		ResultSet result = null;
		List<Order> resultList = new ArrayList<>();
		String sql = "select * from ORDERS where PRODUCT like '%" + name + "%';";
		System.out.println("#: sql=[" + sql + "]");
		try {

			// connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);

			// iterate over results, build resultList
			while (result.next()) {
				Order order = new Order(result.getString("PRODUCT"), result.getString("FIRST_NAME"),
						result.getString("EMAIL"), result.getString("DESCRIPTION"),
						result.getTimestamp("DATE").toString(), "" + result.getDouble("TOTAL"));
				order.setId(result.getInt("ID"));
				resultList.add(order);
			}
			System.out.println("#: Selected orders : " + resultList.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find any Orders. Query=" + sql);
		} finally {

			// attempt to close connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}
}
