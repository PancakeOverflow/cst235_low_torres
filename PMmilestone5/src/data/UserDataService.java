package data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.User;

/**
 * 
 * Data Access object for User
 * 
 * @see IDataService
 */
@Stateless
@Local(IDataService.class)
public class UserDataService implements IDataService<User> {

	private Connection conn;

	/**
	 * Default Constructor
	 */
	public UserDataService() {
		System.out.println("#: UserDataService created");
	}

	@Override
	public boolean create(User user) {
		System.out.println("#: UserDataService.create(" + user.toString() + ")");

		// create query passed User
		String sql = "insert into USER(EMAIL, PASSWORD, FIRST_NAME, LAST_NAME) values ('" + user.getEmail() + "', '"
				+ user.getPassword() + "', '" + user.getFirstName() + "', '" + user.getLastName() + "')";
		boolean status;

		// try to execute query
		try {

			// establish connections
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Create User successful");
			status = true;

		} catch (Exception e) {

			// see if successful
			status = false;
			e.printStackTrace();
			System.out.println("#! Create User failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public User read(User user) {
		System.out.println("#: UserDataService.read(" + user.toString() + ")");

		// create query and result set
		ResultSet result = null;
		User resultUser = null;
		String sql = "select * from USER where EMAIL = '" + user.getEmail() + "' and PASSWORD = '" + user.getPassword()
				+ "'";
		try {

			// establish connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);

			// Create user if match is found
			if (result.next()) {
				resultUser = new User(result.getString("FIRST_NAME"), result.getString("LAST_NAME"),
						result.getString("EMAIL"), result.getString("PASSWORD"), result.getInt("ID"));
			}
			System.out.println("#: Successfully matched user credentials with DB entry. Log in may continue.");
		} catch (SQLException e) {
			System.out.println("#! Failed to find User. Query=" + sql);
		} finally {

			// attempt to close connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultUser;
	}

	@Override
	public boolean update(User user) {
		System.out.println("#: UserDataService.update(" + user.toString() + ")");

		// create query from passed in User
		String sql = "update USER where ID = " + user.getId() + " set EMAIL = '" + user.getEmail() + "', PASSWORD = '"
				+ user.getPassword() + "', FIRST_NAME = '" + user.getFirstName() + "', LAST_NAME = '"
				+ user.getLastName() + "')";
		boolean status;
		try {

			// connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Update User successful");
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update User failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public boolean delete(User user) {
		System.out.println("#: UserDataService.delete(" + user.toString() + ")");

		// create query from passed in User
		String sql = "delete from USER where EMAIL = '" + user.getEmail() + "' and PASSWORD = '" + user.getPassword()
				+ "' and FIRST_NAME = '" + user.getFirstName() + "' and LAST_NAME = '" + user.getLastName() + "')";
		boolean status;
		try {

			// create connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Delete User successful");
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Delete User failed Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public List<User> selectAll() {
		System.out.println("#: UserDataService.selectAll()");

		// create resultSet and query
		ResultSet result = null;
		List<User> resultList = null;
		String sql = "select * from USER";
		try {

			// connect to database
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);
			System.out
					.println("#: Successfully returned a ResultSet containing USER entries. Converting to User list:");
			resultList = new ArrayList<>();

			// build User resultList
			while (result.next()) {
				User u = new User(result.getString("FIRST_NAME"), result.getString("LAST_NAME"),
						result.getString("EMAIL"), result.getString("PASSWORD"), result.getInt("ID"));
				resultList.add(u);
			}
		} catch (SQLException e) {
			System.out.println("#! Failed to select Users. Query=" + sql);
		} finally {

			// attempt to close connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}

}
