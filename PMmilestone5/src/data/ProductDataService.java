package data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import beans.Product;

/**
 * When creating objects that modify a database, this interface should be
 * implemented (CRUD functionality)
 * 
 * @see IDataService
 */
@Stateless
@Local(IDataService.class)
public class ProductDataService implements IDataService<Product> {

	private Connection conn;

	/**
	 * Default constructor
	 */
	public ProductDataService() {
		// TODO Auto-generated constructor stub
		System.out.println("#: ProductDataService created");
	}

	// CRUD
	@Override
	public boolean create(Product product) {
		System.out.println("#: ProductDataService.create(" + product.toString() + ")");

		// create query from passed through Product
		String sql = "insert into PRODUCT(NAME, PRICE, DESCRIPTION, IMAGE) values ('" + product.getName() + "', '"
				+ product.getPrice() + "', '" + product.getDescription() + "', '" + product.getImage() + "')";
		boolean status;
		try {

			// establish connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			System.out.println("#: ProductDataService => Connection open.");
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Inserted product: " + product.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Create Product failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public Product read(Product product) {
		System.out.println("#: ProductDataService.read(" + product.toString() + ")");

		// create query and result set
		ResultSet result = null;
		Product resultProduct = null;
		String sql = "select * from PRODUCT where ID = '" + product.getId() + "'";
		try {

			// establish connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);

			// create resultProduct
			if (result.next()) {
				resultProduct = new Product(result.getString("NAME"), result.getString("DESCRIPTION"),
						result.getString("IMAGE"), result.getString("PRICE"), result.getInt("ID"));
			}
			System.out.println("#: Selected product : " + resultProduct.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find User. Query=" + sql);
		} finally {

			// close connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultProduct;
	}

	@Override
	public boolean update(Product product) {
		System.out.println("#: ProductDataService.update(" + product.toString() + ")");

		// create query from passed through product
		String sql = "update PRODUCT set NAME = '" + product.getName() + "', PRICE = '" + product.getPrice()
				+ "', DESCRIPTION = '" + product.getDescription() + "', IMAGE = '" + product.getImage()
				+ "' where ID = " + product.getId();
		boolean status;
		try {

			// create connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Updated product: " + product.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update Product failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public boolean delete(Product product) {
		System.out.println("#: ProductDataService.delete(" + product.toString() + ")");

		// create query from passed through Product
		String sql = "delete from PRODUCT where ID = " + product.getId();
		boolean status;
		try {

			// establish connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// exectue query
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);
			System.out.println("#: Updated product: " + product.getId());
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			System.out.println("#! Update Product failed. Query=" + sql);
		} finally {

			// attempt to close connection
			if (conn != null)
				try {
					conn.close();
					System.out.println("#: Connection closed");
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("#! Connection failed to close... did the connection actually exist?");
				}
		}
		return status;
	}

	@Override
	public List<Product> selectAll() {
		System.out.println("#: ProductDataService.selectAll()");

		// create resultSet and query
		ResultSet result = null;
		List<Product> resultList = new ArrayList<>();
		String sql = "select * from PRODUCT";
		try {

			// create connection
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);

			// execute query
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);

			// build Product resultList
			while (result.next()) {
				resultList.add(new Product(result.getString("NAME"), result.getString("DESCRIPTION"),
						result.getString("IMAGE"), result.getString("PRICE"), result.getInt("ID")));
			}
			System.out.println("#: Selected products : " + resultList.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find any Products. Query=" + sql);
		} finally {

			// try to close connection
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}

	public List<Product> selectAll(String name) {
		System.out.println("#: ProductDataService.selectAll()");
		ResultSet result = null;
		List<Product> resultList = new ArrayList<>();
		String sql = "select * from PRODUCT where NAME like '%" + name + "%';";
		try {
			String url = "jdbc:mysql://localhost/pastrami?user=root&password=root";
			conn = DriverManager.getConnection(url);
			Statement statement = conn.createStatement();
			result = statement.executeQuery(sql);
			while (result.next()) {
				resultList.add(new Product(result.getString("NAME"), result.getString("DESCRIPTION"),
						result.getString("IMAGE"), result.getString("PRICE"), result.getInt("ID")));
			}
			System.out.println("#: Selected orders : " + resultList.toString());
		} catch (SQLException e) {
			System.out.println("#! Failed to find any Products. Query=" + sql);
		} finally {
			try {
				conn.close();
				System.out.println("#: Connection closed successfully");
			} catch (Exception e) {
				System.out.println("#! Failed to close connection");
				e.printStackTrace();
			}
		}
		return resultList;
	}
}
