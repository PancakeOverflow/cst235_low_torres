package beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import business.IProductManager;

/**
 * 
 * Manages an ArrayList of Products.
 * Private Product List 
 * Private, Injected IProductManager 
 */
@ManagedBean
@ViewScoped
public class ProductList {
	private List<Product> list = new ArrayList<>();

	@Inject
	private IProductManager manager;

	/**
	 * Default Constructor
	 */
	public ProductList() {
		System.out.println("#: ProductList object created.");
	}

	/**
	 * Initializes the Product List to the return value of IProductManager.getProducts()
	 * @see IProductManager
	 */
	@PostConstruct
	public void init() {
		System.out.println("#: PostConstruct called init() on ProductList");
		list = manager.getProducts();
	}

	/**
	 * Gets the Product List
	 * @return Product ArrayList
	 */
	public List<Product> getProducts() {
		System.out.println("#: ProductList.getProducts()");
		return list;
	}

	/**
	 * Sets the Product List
	 * @param productList
	 */
	public void setProducts(List<Product> productList) {
		System.out.println("#: ProductList.setProducts(" + productList.toString() + ")");
		this.list = productList;
	}
}
