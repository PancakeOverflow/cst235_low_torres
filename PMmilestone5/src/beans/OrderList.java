package beans;

import java.util.*;

import javax.faces.bean.*;


@ManagedBean
@ViewScoped
public class OrderList {
	private List<Order> list = new ArrayList<>();
	
	/**
	 * Default constructor
	 */
	public OrderList() {
		System.out.println("#: OrderList object created.");
	}

	/**
	 * Gets the Order List
	 * @return Order ArrayList
	 */
	public List<Order> getOrders() {
		System.out.println("#: OrderList.getOrders()");
		return list;
	}

	/**
	 * Sets the Order List
	 * @param orderList
	 */
	public void setOrders(List<Order> orderList) {
		System.out.println("#: OrderList.setOrders(" + orderList.toString() + ")");
		this.list = orderList;
	}
}
