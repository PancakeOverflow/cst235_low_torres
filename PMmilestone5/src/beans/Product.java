/*
 * Authors: Connor, Mick
 */
package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Connor, Mick Product: holds information for client-side product
 *         display
 */
@ManagedBean
@ViewScoped
public class Product {

	@Size(message = "Please fill out all forms", min = 1)
	@NotNull(message = "Don't be null, be cool")
	private String name, description, image;
	@Size(message = "Please fill out all forms", min = 1)
	@NotNull(message = "Don't be null, be cool")
	private String price;
	@NotNull
	private int id;

	/**
	 * Default Product constructor
	 */
	public Product() {
		this.name = "Default Product";
		this.description = "Default product description";
		this.image = "default.png";
		this.price = "$2.00";
		this.id = 0;
	}

	/**
	 * Product construct by ID: Used to query product information from a database
	 * 
	 * @param id
	 */
	public Product(int id) {
		this.id = id;
	}

	/**
	 * Product constructor
	 * 
	 * @param name
	 * @param description
	 * @param image
	 * @param price
	 * @param id
	 */
	public Product(String name, String description, String image, String price, int id) {
		this.name = name;
		this.description = description;
		this.image = image; // imageName.type; no directory information
		this.price = price;
		this.id = id;
	}

	/**
	 * Get the Product name
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the Product Name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the Product description
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the Product Description
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the file name (with file type) of a product image
	 * 
	 * @return String
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Set the Product Image string
	 * 
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Get the Product price
	 * 
	 * @return String
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * Set the Product Price
	 * 
	 * @param price
	 */
	public void setPrice(String price) {
		
		// Remove $ if present
		if(price.charAt(0) == '$')
			price = price.substring(1);
		this.price = price;
	}

	/**
	 * Get the Product ID
	 * 
	 * @return int
	 */
	public int getId() {
		return id;
	}

	/**
	 * Set the Product ID
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [name=" + name + ", description=" + description + ", image=" + image + ", price=" + price
				+ ", id=" + id + "]";
	}

}
