package beans;

import javax.faces.bean.*;
import javax.validation.constraints.*;

@ManagedBean
@ViewScoped
public class Order {

	@NotNull(message = "Don't be null, be cool")
	private int id;

	@Size(message = "Please fill out all forms", min = 1)
	@NotNull(message = "Don't be null, be cool")
	private String product, firstName, email;

	private String description, date;
	private String price;

	/**
	 * 
	 * @param id
	 * @param productName
	 * @param firstName
	 * @param email
	 * @param description
	 * @param date
	 * @param price
	 */
	public Order(String productName, String firstName, String email, String description, String date, String price) {
		super();
		this.product = productName;
		this.firstName = firstName;
		this.email = email;
		this.description = description;
		this.date = date;
		this.price = price;
	}

	/**
	 * Constructor
	 * 
	 * @param product
	 * @param firstName
	 * @param email
	 * @param price
	 */
	public Order(String product, String firstName, String email, String price) {
		super();
		this.product = product;
		this.firstName = "test first name";
		this.email = email;
		this.price = price;
		this.description = "This is a new Order";
		this.date = "Right now";
	}

	/**
	 * Default constructor;
	 */
	public Order() {

	}

	public Order(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @param product
	 */
	public void setProduct(String product) {
		this.product = product;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 */
	public void setPrice(String price) {
		this.price = price;
	}

}
