/*
 * @author(s): 		Mick Torres, Connor Low
 * @course: 		CST-235
 * @assignment: 	Milestone 2
 * @note: 			this is our own work
 */

package beans;

import javax.faces.bean.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Email;

/**
 * Synopsis: holds client-side information about a logged-in User
 */
@ManagedBean
@SessionScoped
public class User {

	@NotNull(message = "please enter a valid first name.")
	@Size(message = "first name must contain at least 2 characters", min = 2, max = 20)
	private String firstName = "";

	@NotNull(message = "please enter a valid last name.")
	@Size(message = "last name must contain at least 2 characters", min = 2, max = 20)
	private String lastName = "";

	@NotNull(message = "please enter a valid email.")
	@Email(message = "please enter a valid email.")	// Resricts input to be of form email@domain.tld
	@Size(message = "Email must not be blank", min = 5, max = 30)
	private String email = "";

	@NotNull(message = "please enter a valid password.")
	@Size(message = "password must contain at least 5 characters", min = 5, max = 20)
	private String password = "";
	
	private int Id = -1;

	/**
	 * @return the id
	 */
	public int getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		Id = id;
	}

	/**
	 * Default User constructor
	 */
	public User() {
		this.email = "";
		this.password = "";
		this.firstName = "";
		this.lastName = "";
	}

	/**
	 * User Constructor
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param password
	 */
	public User(String firstName, String lastName, String email, String password, int Id) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.Id = Id;
	}

	/**
	 * User constructor for login
	 * @param email
	 * @param password
	 */
	public User(String email, String password) {
		this.email = email;
		this.password = password;
		this.firstName = "";
		this.lastName = "";
	}

	/**
	 * First Name
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * First Name
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Last Name
	 * @return String
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * Last Name
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Email
	 * @return String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Password
	 * @return String
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
