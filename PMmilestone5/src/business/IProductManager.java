/*
 * Authors: Connor, Mick
 */
package business;

import java.util.List;

import javax.ejb.Local;

import beans.Product;

/**
 * Used to define classes that manage products for a client. 
 *
 */
@Local
public interface IProductManager {

	/**
	 * Products set the list of products contained in a local ArrayList
	 * 
	 * @param productList
	 */
	public void setProducts(List<Product> productList);

	/**
	 * Gets a Product List
	 * 
	 * @return Product ArrayList
	 */
	public List<Product> getProducts();

	/**
	 * Adds a product to a local ArrayList
	 * 
	 * @param p
	 */
	public void addProduct(Object p);

	/**
	 * Removes a product from a local ArrayList;
	 */
	public void removeProduct(Object p);
}
