package business;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.*;

import beans.Product;
import data.ProductDataService;

/**
 * A Business Service for the Product Class.
 *
 */
@Stateless
@Local
public class ProductBusinessService {

	private Product product;

	private ProductDataService service = new ProductDataService();;

	/**
	 * Default Constructor
	 */
	public ProductBusinessService() {
		System.out.println("#: ProductBusinessService created");
	}

	/**
	 * Constructor
	 * 
	 * @param product
	 */
	public ProductBusinessService(Product product) {
		System.out.println("#: ProductBusinessService created with product " + product.toString());
		this.product = product;
	}

	/**
	 * Interacts with a ProductDataService to insert a product into the database
	 */
	public void createProduct() {
		System.out.println("#: ProductBusinessService.createProduct()");

		// Check product information lengths
		if (product != null && product.getName().length() <= 200 && product.getImage().length() <= 255) {
			service.create(product);
		}
	}

	/**
	 * Interacts with a ProductDataService to delete a product from the database
	 */
	public void deleteProduct() {
		System.out.println("#: ProductBusinessService.deleteProduct()");

		// Check product information lengths
		if (product != null && product.getName().length() <= 200 && product.getImage().length() <= 255) {
			service.delete(product);
		}
	}

	/**
	 * Interacts with a ProductDataService to update a product in the database
	 */
	public void updateProduct() {
		System.out.println("#: ProductBusinessService.updateProduct()");

		// Check product information lengths
		if (product != null && product.getName().length() <= 200 && product.getImage().length() <= 255) {
			service.update(product);
		}
	}

	/**
	 * Interacts with a ProductDataService to read a product from the database
	 * 
	 * @return Product result
	 */
	public Product selectProduct() {
		System.out.println("#: ProductBusinessService.selectProduct()");
		Product result = service.read(product);
		System.out.println("#: Selected Product: " + result.toString());
		return result;
	}

	/**
	 * Interacts with a ProductDataService to read a list of products from the
	 * database. Products are selected using a string relating to a Product name
	 * property.
	 * 
	 * @param name
	 * @return
	 */
	public List<Product> selectProductByName(String name) {
		System.out.println("#: ProductBusinessService.selectAllProduct()");
		List<Product> productArray = service.selectAll(name);
		System.out.println("#: SelectAll found " + productArray.size() + " products.");
		return productArray;
	}

	/**
	 * Interacts with a ProductDataService to read a list of products from the
	 * database
	 * 
	 * @return Product ArrayList
	 * @throws SQLException
	 */
	public List<Product> selectAllProducts() throws SQLException {
		System.out.println("#: ProductBusinessService.selectAllProduct()");
		List<Product> productArray = service.selectAll();
		System.out.println("#: SelectAll found " + productArray.size() + " products.");
		return productArray;
	}

	/**
	 * 
	 * @return product
	 */
	public Product getProduct() {
		System.out.println("#: ProductBusinessService.getProduct() returned " + product.toString());
		return product;
	}

	/**
	 * @param product
	 */
	public void setProduct(Product product) {
		System.out.println("#: ProductBusinessService.setProduct(" + product.toString() + ")");
		this.product = product;
	}

}
