package business;

import javax.ejb.*;

import beans.User;
import data.UserDataService;

/**
 * Business Rules for the User Class
 */
@Stateless
@Local
public class UserBusinessService {
	private User user;

	private UserDataService service = new UserDataService();

	/**
	 * Default Constructor
	 */
	public UserBusinessService() {

	}

	/**
	 * Constructor
	 * 
	 * @param user
	 */
	public UserBusinessService(User user) {
		this.user = user;
	}

	/**
	 * Validates User email and password with Database. Maps to the DAO read method.
	 * 
	 * @return User
	 */
	public User loginUser() {
		System.out.println("#: loginUser() called");

		// check if username and password match basic requirements
		// Max length is 100
		if (user.getEmail().length() > 3 && user.getPassword().length() > 4 && user.getEmail().length() < 100
				&& user.getPassword().length() < 100) {
			System.out.println("#: returning result set...");
			user = service.read(user);
			return user;
		}

		// if failed to find a user, return null
		System.out.println(
				"#! loginUser() failed: rules broken: email=" + user.getEmail() + " password=" + user.getPassword());
		return null;
	}

	/**
	 * Validates User object by applying business rules and then sends information
	 * to a UserDataService. Maps to the DAO object create method.
	 * 
	 * @return boolean
	 */
	public boolean registerUser() {
		System.out.println("#: registerUser() called");

		// check if User credentials match basic requirements
		// Max length is 100
		if (user.getEmail().length() > 3 && user.getPassword().length() > 4 && user.getFirstName().length() > 1
				&& user.getLastName().length() > 1 && user.getEmail().length() < 100
				&& user.getPassword().length() < 100 && user.getFirstName().length() < 100
				&& user.getLastName().length() < 100) {
			System.out.println("#: attempting to add User");
			return service.create(user);
		}
		System.out.println("#! loginUser() failed: rules broken: email=" + user.getEmail() + " password="
				+ user.getPassword() + " firstName=" + user.getFirstName() + " lastname=" + user.getLastName());
		return false;
	}

}
