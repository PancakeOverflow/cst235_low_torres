package business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import beans.Product;

/**
 * A Rest Service for the Product Class
 *
 */
@RequestScoped
@Path("/products")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ProductsRestService {

	@EJB
	ProductBusinessService service;

	/**
	 * A REST GET method that returns a list of Products as JSON
	 * 
	 * @param id
	 * @param name
	 * @return Product List as JSON
	 * @throws SQLException
	 */
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Product> getProductsAsJson(@DefaultValue("0") @QueryParam("id") int id,
			@DefaultValue("") @QueryParam("name") String name) throws SQLException {

		// check if a specific product is being selected
		// create a return list
		List<Product> list = new ArrayList<>();
		if (id <= 1) {
			if (name.length() > 0) {
				list.addAll(service.selectProductByName(name));
				return list;
			}
			return service.selectAllProducts();
		}

		// select product by id
		service.setProduct(new Product(id));
		list.add(service.selectProduct());
		return list;
	}
}
