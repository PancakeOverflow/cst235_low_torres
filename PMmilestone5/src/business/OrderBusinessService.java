package business;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.*;

import beans.Order;
import data.OrderDataService;

/**
 * 
 * A Business Service for the Order class.
 */
@Stateless
@Local
public class OrderBusinessService {

	private Order order;

	private OrderDataService service = new OrderDataService();;

	/**
	 * Default Constructor
	 */
	public OrderBusinessService() {
		System.out.println("#: OrderBusinessService created");
	}

	/**
	 * Constructor
	 * 
	 * @param order
	 */
	public OrderBusinessService(Order order) {
		System.out.println("#: OrderBusinessService created with order " + order.toString());
		this.order = order;
	}

	/**
	 * Interacts with a OrderDataService to insert a order into the database
	 */
	public void createOrder() {
		System.out.println("#: OrderBusinessService.createOrder()");
		if (order != null && order.getProduct().length() <= 200 && order.getDescription().length() <= 25500) {
			service.create(order);
		}
	}

	/**
	 * Interacts with a OrderDataService to delete a order from the database
	 */
	public void deleteOrder() {
		System.out.println("#: OrderBusinessService.deleteOrder()");
		if (order != null && order.getProduct().length() <= 200 && order.getDescription().length() <= 25500) {
			service.delete(order);
		}
	}

	/**
	 * Interacts with a OrderDataService to update a order in the database
	 */
	public void updateOrder() {
		System.out.println("#: OrderBusinessService.updateOrder()");
		if (order != null && order.getProduct().length() <= 200 && order.getDescription().length() <= 25500) {
			service.update(order);
		}
	}

	/**
	 * Interacts with a OrderDataService to read a order from the database
	 * 
	 * @return Order result
	 */
	public Order selectOrder() {
		System.out.println("#: OrderBusinessService.selectOrder()");
		Order result = service.read(order);
		System.out.println("#: Selected Order: " + result.toString());
		return result;
	}

	/**
	 * Interacts with a OrderDataService to read a Order List from the database.
	 * Orders are selected using a String mapping to the Order name property.
	 * 
	 * @param name
	 * @return
	 */
	public List<Order> selectOrderByName(String name) {
		System.out.println("#: OrderBusinessService.selectAllOrder()");
		List<Order> orderArray = service.selectAll(name);
		System.out.println("#: SelectAll found " + orderArray.size() + " orders.");
		return orderArray;
	}

	/**
	 * Interacts with a OrderDataService to read a list of orders from the database
	 * 
	 * @return ArrayList<Order>()
	 * @throws SQLException
	 */
	public List<Order> selectAllOrders() throws SQLException {
		System.out.println("#: OrderBusinessService.selectAllOrder()");
		List<Order> orderArray = service.selectAll();
		System.out.println("#: SelectAll found " + orderArray.size() + " orders.");
		return orderArray;
	}

	/**
	 * 
	 * @return order
	 */
	public Order getOrder() {
		System.out.println("#: OrderBusinessService.getOrder() returned " + order.toString());
		return order;
	}

	/**
	 * 
	 * @param order
	 */
	public void setOrder(Order order) {
		System.out.println("#: OrderBusinessService.setOrder(" + order.toString() + ")");
		this.order = order;
	}

}
