package business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import beans.Order;

/**
 * 
 * A Rest Service for the Order class.
 */
@RequestScoped
@Path("/orders")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class OrdersRestService {

	@EJB
	OrderBusinessService service;

	/**
	 * Uses the REST GET method to return a JSON list of Orders.
	 * 
	 * @param id
	 * @param name
	 * @return Order List
	 * @throws SQLException
	 */
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Order> getOrdersAsJson(@DefaultValue("0") @QueryParam("id") int id,
			@DefaultValue("") @QueryParam("product_name") String name) throws SQLException {
		// create a return list
		List<Order> list = new ArrayList<>();

		// check if a specific order is being selected
		if (id < 1) {
			if (name.length() > 0) {
				list.addAll(service.selectOrderByName(name));
				return list;
			}
			return service.selectAllOrders();
		}

		// select order by id
		System.out.println("--------------------------------hey");
		service.setOrder(new Order(id));
		list.add(service.selectOrder());
		return list;
	}
}
