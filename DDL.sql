-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pastrami
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DESCRIPTION` text,
  `PRODUCT` varchar(200) DEFAULT NULL,
  `TOTAL` double DEFAULT NULL,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2017-12-04 05:49:13','This is a new Order','JOHN CENA MEME',500,'test first name','connorjameslow@gmail.com'),(2,'2017-12-04 06:07:41','This is a new Order','JOHN CENA MEME',500,'test first name',''),(3,'2017-12-04 06:07:44','This is a new Order','danger Product',1.99,'test first name',''),(4,'2017-12-04 06:07:47','This is a new Order','danger Product',1.99,'test first name',''),(5,'2017-12-04 06:11:45','This is a new Order','Default Product',20,'test first name',''),(6,'2017-12-04 06:38:57','This is a new Order','danger Productt',1.98,'test first name','connorjameslow@gmail.com'),(7,'2017-12-04 06:47:00','This is a new Order','JOHN CENA MEME',500,'test first name','user@email.notreal'),(8,'2017-12-04 06:47:15','This is a new Order','Added product',2,'test first name','user@email.notreal'),(9,'2017-12-04 06:47:38','This is a new Order','danger Productt',1.98,'test first name','user@email.notreal'),(10,'2017-12-11 19:39:53','This is a new Order','Default another word',20,'test first name','connorjameslow@gmail.com'),(11,'2017-12-11 19:44:11','This is a new Order','Default another word',20,'test first name',''),(12,'2017-12-11 19:55:58','This is a new Order','Default another word',20,'test first name','e@g.c'),(13,'2017-12-11 19:56:12','This is a new Order','jon sena',9001,'test first name','e@g.c');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) NOT NULL,
  `PRICE` double NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `IMAGE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (4,'danger Producttt',1.98,'Defa','default.png'),(5,'Added product',2,'Default product description','default.png'),(6,'jon sena',9001,'Default product description','default.png'),(7,'I made this one while they had errors',100,'only hulk','default.png'),(8,'dunder mifflin',404,'Default product description','default.png');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(100) NOT NULL,
  `ADMIN` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Connor','Low','clow2@my.gcu.edu','1234cccc',1),(2,'Connor','Low','connorjameslow@gmail.com','1234cccc',0),(3,'john','cena','you@cant.see','me:cena',0),(4,'MOM','STOP','COMMENTING@ON.MY','VIDEOS',0),(5,'Connor','Low','connorjameslow2@gmail.com','abcde',0),(6,'dog','farts','tootsincommon@some.site','hidad',0),(7,'dog','bones','newemail@hi.mom','1234cccc',0),(8,'connor','but different','new@email.com','567890',0),(9,'another','user','user@email.notreal','fakenews',0),(10,'another','newuser','emails@g.c','ajvjkaispdvokj',0),(11,'new','user','uaerjsdk@gm.cslk','asdfghjkl',0),(12,'For','Fun','e@g.c','Password',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-14 18:22:33
